package com.order.service.serviceApps;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.order.service.converters.OrderConverter;
import com.order.service.dtos.OrderDto;
import com.order.service.entities.Order;
import com.order.service.services.OrderService;

/**
 * @author miry
 *
 */
@Service
public class OrderServiceApp {

	@Autowired
	private OrderService orderService;
	@Autowired
	private OrderConverter orderConverter;
	
	public List<OrderDto> findByOrderId(String orderId){
		List<OrderDto> results = null;
		try {
			List<Order> orders = orderService.findByOrderId(orderId);
			if (orders != null) {
				results = orders.stream().map(o -> orderConverter.convertToDto(o)).collect(Collectors.toList());
			}
		} catch (Exception ex) {
			System.out.println("error::" + ex.getMessage());
		}
		return results;
	}

	public OrderDto saveData(OrderDto orderDto) {
		OrderDto result = null;
		try {
			if (orderDto == null) {
				throw new NullPointerException("cannot save empty Order");
			}

			Order order = orderService.saveData(orderConverter.convertToEntity(orderDto));
			result = orderConverter.convertToDto(order);

		} catch (Exception ex) {
			System.out.println("error::" + ex.getMessage());
		}
		return result;
	}

	public OrderDto findById(String id) {
		Order order = null;
		OrderDto result = null;
		try {
			order = orderService.findById(id);
			if (order != null) {
				result = orderConverter.convertToDto(order);
			}
		} catch (Exception ex) {
			System.out.println("error::" + ex.getMessage());
		}

		return result;
	}

	public List<OrderDto> findByBuyerEmail(String buyerEmail) {
		List<OrderDto> results = null;
		try {
			List<Order> orders = orderService.findByBuyerEmail(buyerEmail);
			if (orders != null) {
				results = orders.stream().map(o -> orderConverter.convertToDto(o)).collect(Collectors.toList());
			}
		} catch (Exception ex) {
			System.out.println("error::" + ex.getMessage());
		}
		return results;
	}

	public Page<OrderDto> findData(Pageable pageable) {
		List<OrderDto> dtos = null;
		Page<OrderDto> results = null;
		try {
			Page<Order> orderPage = orderService.findData(pageable);
			if (orderPage != null) {
				dtos = orderPage.getContent().stream().map(o -> orderConverter.convertToDto(o))
						.collect(Collectors.toList());
				results = new PageImpl<OrderDto>(dtos, pageable, orderPage.getTotalElements());
			}
		} catch (Exception ex) {
			System.out.println("error::" + ex.getMessage());
		}
		return results;
	}

}
