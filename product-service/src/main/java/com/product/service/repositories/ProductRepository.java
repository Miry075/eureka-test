/**
 * 
 */
package com.product.service.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.product.service.entities.Product;

/**
 * @author miry
 *
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long>{

}
