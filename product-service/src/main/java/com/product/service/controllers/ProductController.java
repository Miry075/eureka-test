package com.product.service.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.product.service.dtos.ProductDto;
import com.product.service.serviceApps.ProductServiceApp;

/**
 * @author miry
 *
 */

@RestController
public class ProductController {

	@Autowired
	private ProductServiceApp productService;

	@GetMapping(value = "/products")
	public Page<ProductDto> getProducts(Pageable pageable) {
		return productService.findData(pageable);
	}

}
