/**
 * @author miry
 *
 */
package com.order.service.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.order.service.dtos.OrderDto;
import com.order.service.serviceApps.OrderServiceApp;

/**
 * @author miry
 *
 */

@RestController
public class OrderController {

	@Autowired
	private OrderServiceApp orderServiceApp;

	@GetMapping(value = "/orders/order/{orderId}")
	public List<OrderDto> findByOrderId(@PathVariable("orderId") String orderId) {
		return orderServiceApp.findByOrderId(orderId);
	}

	@PostMapping(value = "/order")
	public OrderDto saveData(@RequestBody OrderDto orderDto) {
		return orderServiceApp.saveData(orderDto);
	}

	@GetMapping(value = "/order/{id}")
	public OrderDto findById(@PathVariable("id") String id) {
		return orderServiceApp.findById(id);
	}

	@GetMapping(value = "/orders/buyer/{buyerEmail}")
	public List<OrderDto> findByBuyerEmail(@PathVariable("buyerEmail") String buyerEmail) {
		return orderServiceApp.findByBuyerEmail(buyerEmail);
	}
}
