package com.product.service.dtos;

import java.time.LocalDate;

import com.product.service.entities.Product;

/**
 * @author miry
 *
 */

public class ProductDto {

	private String id;
	private String name;
	private String description;
	private Double price;
	private LocalDate creationDate;

	public ProductDto(Long id, String name, String description, Double price, LocalDate creationDate) {
		this.id = id.toString();
		this.name = name;
		this.description = description;
		this.price = price;
		this.creationDate = creationDate;
	}

	public ProductDto(Product product) {
		this.id = product.getId().toString();
		this.name = product.getName();
		this.description = product.getDescription();
		this.price = product.getPrice();
		this.creationDate = product.getCreationDate();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public LocalDate getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}

}
