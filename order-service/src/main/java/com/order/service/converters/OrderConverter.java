/**
 * @author miry
 *
 */
package com.order.service.converters;

import org.springframework.stereotype.Component;

import com.order.service.dtos.OrderDto;
import com.order.service.entities.Order;

/**
 * @author miry
 *
 */
@Component
public class OrderConverter {

	public Order convertToEntity(OrderDto dto) {
		Order order = new Order();
		order.setId(dto.getId());
		order.setDateAndTimeOfOrder(dto.getDateAndTimeOfOrder());
		order.setBuyerEmail(dto.getBuyerEmail());
		order.setProductId(dto.getProductId());
		order.setOrderId(dto.getOrderId());
		return order;
	}

	public OrderDto convertToDto(Order order) {
		return new OrderDto(order.getId(), order.getDateAndTimeOfOrder(), order.getBuyerEmail(), order.getProductId(), order.getOrderId());
	}
}
