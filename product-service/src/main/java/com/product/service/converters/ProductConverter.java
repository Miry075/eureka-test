package com.product.service.converters;

import org.springframework.stereotype.Component;

import com.product.service.dtos.ProductDto;
import com.product.service.entities.Product;

/**
 * @author miry
 *
 */

@Component
public class ProductConverter {
	
	public Product convertToEntity(ProductDto dto) {
		Product p = new Product();
		if (dto != null) {
			p.setName(dto.getName());
			p.setDescription(dto.getDescription());
			p.setPrice(dto.getPrice());
			p.setCreationDate(dto.getCreationDate());
		}
		return p;
	}
	
	
	public ProductDto convertToDto(Product p) {
		return new ProductDto(p);
	}
	
}
