package com.order.service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import com.order.service.entities.Order;
import com.order.service.repositories.OrderRepository;

@EnableEurekaClient
@SpringBootApplication
public class OrderServiceApplication implements CommandLineRunner {

	@Autowired
	private OrderRepository orderRepository;

	public static void main(String[] args) {
		SpringApplication.run(OrderServiceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO remove on production
		dataInitilizer();

	}

	private void dataInitilizer() {
		Long orderCount = orderRepository.count();
		int nbOrder = 10;
		if (orderCount == 0) {
			for (int i = 0; i < nbOrder; i++) {
				Order order = new Order();
				order.setBuyerEmail("hery.mirindra@outlook.com");
				order.setDateAndTimeOfOrder(LocalDateTime.now());
				order.setOrderId("ORDERID" + i);
				for (int x = 0; x < 3; x++) {
					Random r = new Random();
					Integer productId = r.nextInt(10);
					order.setProductId(productId.toString());
					orderRepository.save(order);
				}
			}
		}

	}

}
