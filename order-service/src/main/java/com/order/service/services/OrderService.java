/**
 * @author miry
 *
 */
package com.order.service.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.order.service.communs.services.ServiceMetier;
import com.order.service.communs.services.ServiceMetierImpl;
import com.order.service.entities.Order;
import com.order.service.repositories.OrderRepository;

/**
 * @author miry
 *
 */
@Service
public class OrderService extends ServiceMetierImpl<Order, Long, OrderRepository>
		implements ServiceMetier<Order, Long> {

	public List<Order> findByOrderId(String orderId) {
		return this.repository.findByOrderId(orderId);
	}

	public Order findById(String id) {
		return this.repository.findById(id);
	}

	public List<Order> findByBuyerEmail(String email) {
		return this.repository.findByBuyerEmail(email);
	}

}
