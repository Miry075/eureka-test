package com.product.service.services;

import org.springframework.stereotype.Service;

import com.product.service.communs.services.ServiceMetier;
import com.product.service.communs.services.ServiceMetierImpl;
import com.product.service.entities.Product;
import com.product.service.repositories.ProductRepository;

/**
 * @author miry
 *
 */

@Service
public class ProductService extends ServiceMetierImpl<Product, Long, ProductRepository> implements ServiceMetier<Product, Long>{

}
