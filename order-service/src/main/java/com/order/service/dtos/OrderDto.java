/**
 * @author miry
 *
 */
package com.order.service.dtos;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author miry
 *
 */
public class OrderDto {

	private Long id;
	private LocalDateTime dateAndTimeOfOrder;
	private String buyerEmail;
	private String productId;
	private String orderId;

	public OrderDto(Long id, LocalDateTime dateOfOrder, String buyerEmail, String productId, String orderId) {
		this.id = id;
		this.dateAndTimeOfOrder = dateOfOrder;
		this.buyerEmail = buyerEmail;
		this.productId = productId;
		this.orderId = orderId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public LocalDateTime getDateAndTimeOfOrder() {
		return dateAndTimeOfOrder;
	}

	public void setDateAndTimeOfOrder(LocalDateTime dateAndTimeOfOrder) {
		this.dateAndTimeOfOrder = dateAndTimeOfOrder;
	}

	public String getBuyerEmail() {
		return buyerEmail;
	}

	public void setBuyerEmail(String buyerEmail) {
		this.buyerEmail = buyerEmail;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}
}
