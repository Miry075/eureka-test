
package com.order.service.entities;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author miry
 *
 */

@Entity
@Table(name = "orders")
public class Order {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "date_and_time_of_order")
	private LocalDateTime dateAndTimeOfOrder;
	
	@Column(name = "buyer_email")
	private String buyerEmail;
	
	@Column(name = "product_id")
	private String productId;
	
	@Column(name = "order_id")
	private String orderId;
	
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDateAndTimeOfOrder() {
		return dateAndTimeOfOrder;
	}

	public void setDateAndTimeOfOrder(LocalDateTime dateOfOrder) {
		this.dateAndTimeOfOrder = dateOfOrder;
	}

	public String getBuyerEmail() {
		return buyerEmail;
	}

	public void setBuyerEmail(String buyerEmail) {
		this.buyerEmail = buyerEmail;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}


	
}
