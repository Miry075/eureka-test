/**
 * @author miry
 *
 */
package com.order.service.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.order.service.entities.Order;

/**
 * @author miry
 *
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
	
	public List<Order> findByOrderId(String orderId);
	public Order findById(String id);
	public List<Order> findByBuyerEmail(String buyerEmail);
	
	
}
