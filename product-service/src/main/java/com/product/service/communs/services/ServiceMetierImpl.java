package com.product.service.communs.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author miry
 *
 */
public class ServiceMetierImpl<T, TIdentity, TRepository extends JpaRepository<T, TIdentity>> implements ServiceMetier<T, TIdentity>{
	
	@Autowired
	protected TRepository repository;

	@Override
	public Page<T> findData(Pageable page) {
		return repository.findAll(page);
	}

	@Override
	public T saveData(T entity) {
		return repository.save(entity);
	}

	@Override
	public Long count() {
		return repository.count();
	}

}
