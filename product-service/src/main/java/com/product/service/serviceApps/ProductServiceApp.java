/**
 * @author miry
 *
 */
package com.product.service.serviceApps;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.product.service.converters.ProductConverter;
import com.product.service.dtos.ProductDto;
import com.product.service.entities.Product;
import com.product.service.services.ProductService;

/**
 * @author miry
 *
 */

@Service
public class ProductServiceApp {

//	private final Logger LOGGER = LoggerFactory.getLogger(ProductServiceApp.class);

	@Autowired
	private ProductService productService;
	@Autowired
	private ProductConverter productConverter;

	public Page<ProductDto> findData(Pageable pageable) {
		List<ProductDto> dtos = null;
		Page<ProductDto> results = null;
		try {
			Page<Product> productPage = productService.findData(pageable);
			if (productPage != null) {
				dtos = productPage.getContent().stream().map(p -> productConverter.convertToDto(p))
						.collect(Collectors.toList());
				results = new PageImpl<ProductDto>(dtos, pageable, productPage.getTotalElements());
			}

		} catch (Exception ex) {
			System.out.println("error::" + ex.getMessage());
		}
		return results;

	}

	@Transactional
	public ProductDto saveData(ProductDto dto) {
		ProductDto result = null;
		try {
			if (dto == null) {
				throw new NullPointerException("cannot save empty Product");
			}
			Product product = productService.saveData(productConverter.convertToEntity(dto));
			result = productConverter.convertToDto(product);

		} catch (Exception ex) {
			System.out.println("error::" + ex.getMessage());
		}
		return result;
	}

}
