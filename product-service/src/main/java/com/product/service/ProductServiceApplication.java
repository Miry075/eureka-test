package com.product.service;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import com.product.service.entities.Product;
import com.product.service.repositories.ProductRepository;

@EnableEurekaClient
@SpringBootApplication
public class ProductServiceApplication implements CommandLineRunner {

	@Autowired
	private ProductRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(ProductServiceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO remove on Production
		dataInitializer();
	}

	private void dataInitializer() throws Exception {
		Long countProduct = repository.count();
		int nbProduct = 10;
		try {
			if (countProduct == 0) {
				for (int i = 0; i < nbProduct; i++) {
					Product p = new Product();
					p.setName("Product_" + i);
					p.setDescription("Description_" + i);
					p.setPrice((double) 350);
					p.setCreationDate(LocalDate.now());

					repository.save(p);
				}
			}

		} catch (Exception ex) {
			throw ex;
		}

	}

}
