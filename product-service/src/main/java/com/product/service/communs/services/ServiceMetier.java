package com.product.service.communs.services;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author miry
 *
 */
public interface ServiceMetier<T, TIdentity> {
	
	public Page<T> findData(Pageable page);
	public T saveData(T entity);
	public Long count();
	
}
